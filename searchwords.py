#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sortwords
import sys

def search_word(word, words_list):
    found = False
    for i in words_list:
        if sortwords.equal(word, i):
            test_index = words_list.index(i)
            found = True
            return test_index

    if not found:
        raise Exception

def main():
    words_list = sys.argv[1:]
    word = sys.argv[5]

    if len(sys.argv) < 3:
        sys.exit("At least two arguments are needed")

    try:
        ordered_list = sortwords.sort(words_list)
        position = search_word(word, ordered_list)
        sortwords.show(ordered_list)
        print(position)
    except Exception:
        sys.exit("Word not found")

if __name__ == '__main__':
    main()
